#include "LedControl.h"
#include <Time.h>
#include <TimeLib.h>
#include <Wire.h>
#include <DS1307RTC.h>
#include <LedControl.h>

#define MODE_TIME 1
#define MODE_HOURS 2
#define MODE_MINUTES 3

/*

  pin 12 is connected to the DataIn
  pin 11 is connected to the CLK
  pin 10 is connected to LOAD
  We have only a single MAX72XX.
*/

int MODE = MODE_MINUTES;

int flagTime = 0;
int flagHourUp = 0;
int flagHourDown = 0;
int flagMinuteUp = 0;
int flagMinuteDown = 0;

int buttonModePin = 6;
int buttonUpPin = 5;
int buttonDownPin = 4;

int buttonModeState = 0;
int buttonUpState = 0;
int buttonDownState = 0;

//int photocellPin = 0;     // the cell and 10K pulldown are connected to a0, Пин для дптчика очвещенности
//int photocellReading; // Значение с датчика освещенности
//int bright; // Яркость

const int DISPLAY_COUNT = 4; //Количество дисплеев

LedControl lc = LedControl(12, 11, 10, DISPLAY_COUNT); //Инициализация дисплеев

/* we always wait a bit between updates of the display */
unsigned long delaytime = 1000; //Задержка

long previousTimeMillis = 0;
long previousHoursMillis = 0;
long previousMinutesMillis = 0;

int shutdownHours = 0;
int shutdownMinutes = 0;

void setup() {
  /*
    The MAX72XX is in power-saving mode on startup,
    we have to do a wakeup call
  */

  Serial.begin(9600);

  pinMode(buttonModePin, INPUT);
  digitalWrite(buttonModePin, HIGH); 

  pinMode(buttonUpPin, INPUT);
  digitalWrite(buttonUpPin, HIGH);

  pinMode(buttonDownPin, INPUT);
  digitalWrite(buttonDownPin, HIGH);
  
  //Структура, содержащая знакчения даты и времени в текущий момент времени
  tmElements_t tm;

  //  photocellReading = analogRead(photocellPin);
  //  photocellReading = adaptiveBrightness(photocellReading);

  Serial.begin(9600);

  /*
     Включение экранов
  */
  lc.shutdown(0, false);
  lc.shutdown(1, false);
  lc.shutdown(2, false);
  lc.shutdown(3, false);

  /*
    Установка яркости
  */
  lc.setIntensity(0, 15);
  lc.setIntensity(1, 15);
  lc.setIntensity(2, 15);
  lc.setIntensity(3, 15);

  /* Set the brightness to a medium values */
  //  lc.setIntensity(0, photocellReading);
  //  lc.setIntensity(1, photocellReading);
  //  lc.setIntensity(2, photocellReading);
  //  lc.setIntensity(3, photocellReading);

  /* and clear the display */
  lc.clearDisplay(0);
  lc.clearDisplay(1);
  lc.clearDisplay(2);
  lc.clearDisplay(3);

  RTC.read(tm); //Инициализация времени

  /*
    Вывод времени на экран
  */
  writeHours(tm.Hour);
  writeMinutes(tm.Minute);
}

/*
   Вывод чисел на экран
   --------------------
*/
void writeTwo(int display) {
  byte two[8] = {B11011111, B11011111, B11011011, B11011011, B11011011, B11011011, B11111011, B11111011};
  lc.setRow(display, 7, two[0]);
  lc.setRow(display, 6, two[1]);
  lc.setRow(display, 5, two[2]);
  lc.setRow(display, 4, two[3]);
  lc.setRow(display, 3, two[4]);
  lc.setRow(display, 2, two[5]);
  lc.setRow(display, 1, two[6]);
  lc.setRow(display, 0, two[7]);
}

void writeThree(int display) {
  byte three[8] = {B11000011, B11000011, B11011011, B11011011, B11011011, B11011011, B11111111, B11111111};
  lc.setRow(display, 7, three[0]);
  lc.setRow(display, 6, three[1]);
  lc.setRow(display, 5, three[2]);
  lc.setRow(display, 4, three[3]);
  lc.setRow(display, 3, three[4]);
  lc.setRow(display, 2, three[5]);
  lc.setRow(display, 1, three[6]);
  lc.setRow(display, 0, three[7]);
}

void writeSix(int display) {
  byte six[8] = {B11111111, B11111111, B11011011, B11011011, B11011011, B11011011, B11011111, B11011111};
  lc.setRow(display, 7, six[0]);
  lc.setRow(display, 6, six[1]);
  lc.setRow(display, 5, six[2]);
  lc.setRow(display, 4, six[3]);
  lc.setRow(display, 3, six[4]);
  lc.setRow(display, 2, six[5]);
  lc.setRow(display, 1, six[6]);
  lc.setRow(display, 0, six[7]);
}

void writeFour(int display) {
  byte four[8] = {B11111000, B11111000, B00011000, B00011000, B00011000, B00011000, B11111111, B11111111};
  lc.setRow(display, 7, four[0]);
  lc.setRow(display, 6, four[1]);
  lc.setRow(display, 5, four[2]);
  lc.setRow(display, 4, four[3]);
  lc.setRow(display, 3, four[4]);
  lc.setRow(display, 2, four[5]);
  lc.setRow(display, 1, four[6]);
  lc.setRow(display, 0, four[7]);
}

void writeFive(int display) {
  byte five[8] = {B11111011, B11111011, B11011011, B11011011, B11011011, B11011011, B11011111, B11011111};
  lc.setRow(display, 7, five[0]);
  lc.setRow(display, 6, five[1]);
  lc.setRow(display, 5, five[2]);
  lc.setRow(display, 4, five[3]);
  lc.setRow(display, 3, five[4]);
  lc.setRow(display, 2, five[5]);
  lc.setRow(display, 1, five[6]);
  lc.setRow(display, 0, five[7]);
}

void writeOne(int display) {
  byte one[8] = {B00000011, B00100011, B01100011, B11111111, B11111111, B00000011, B00000011, B00000011};
  lc.setRow(display, 7, one[0]);
  lc.setRow(display, 6, one[1]);
  lc.setRow(display, 5, one[2]);
  lc.setRow(display, 4, one[3]);
  lc.setRow(display, 3, one[4]);
  lc.setRow(display, 2, one[5]);
  lc.setRow(display, 1, one[6]);
  lc.setRow(display, 0, one[7]);
}

void writeZero(int display) {
  byte zero[8] = {B11111111, B11111111, B11000011, B11000011, B11000011, B11000011, B11111111, B11111111};
  lc.setRow(display, 7, zero[0]);
  lc.setRow(display, 6, zero[1]);
  lc.setRow(display, 5, zero[2]);
  lc.setRow(display, 4, zero[3]);
  lc.setRow(display, 3, zero[4]);
  lc.setRow(display, 2, zero[5]);
  lc.setRow(display, 1, zero[6]);
  lc.setRow(display, 0, zero[7]);
}

void writeSeven(int display) {
  byte seven[8] = {B11000000, B11000000, B11001111, B11001111, B11011000, B11011000, B11110000, B11110000};
  lc.setRow(display, 7, seven[0]);
  lc.setRow(display, 6, seven[1]);
  lc.setRow(display, 5, seven[2]);
  lc.setRow(display, 4, seven[3]);
  lc.setRow(display, 3, seven[4]);
  lc.setRow(display, 2, seven[5]);
  lc.setRow(display, 1, seven[6]);
  lc.setRow(display, 0, seven[7]);
}

void writeEight(int display) {
  byte eight[8] = {B11111111, B11111111, B11011011, B11011011, B11011011, B11011011, B11111111, B11111111};
  lc.setRow(display, 7, eight[0]);
  lc.setRow(display, 6, eight[1]);
  lc.setRow(display, 5, eight[2]);
  lc.setRow(display, 4, eight[3]);
  lc.setRow(display, 3, eight[4]);
  lc.setRow(display, 2, eight[5]);
  lc.setRow(display, 1, eight[6]);
  lc.setRow(display, 0, eight[7]);
}

void writeNine(int display) {
  byte nine[8] = {B11111011, B11111011, B11011011, B11011011, B11011011, B11011011, B11111111, B11111111};
  lc.setRow(display, 7, nine[0]);
  lc.setRow(display, 6, nine[1]);
  lc.setRow(display, 5, nine[2]);
  lc.setRow(display, 4, nine[3]);
  lc.setRow(display, 3, nine[4]);
  lc.setRow(display, 2, nine[5]);
  lc.setRow(display, 1, nine[6]);
  lc.setRow(display, 0, nine[7]);
}

//------------------------------

/*
  Вывод числа на указанный дисплей
*/
void writeNumberOnDisplay(int display, int number) {
  switch (number) {
    case 0:
      writeZero(display);
      break;
    case 1:
      writeOne(display);
      break;
    case 2:
      writeTwo(display);
      break;
    case 3:
      writeThree(display);
      break;
    case 4:
      writeFour(display);
      break;
    case 5:
      writeFive(display);
      break;
    case 6:
      writeSix(display);
      break;
    case 7:
      writeSeven(display);
      break;
    case 8:
      writeEight(display);
      break;
    case 9:
      writeNine(display);
      break;
  }
}

/*
  Вывод минут на дисплей
*/
void writeMinutes(int minute) {

  int number;
  int i = 1;

  /*
     Очищаем экраны
  */
  lc.clearDisplay(3);
  lc.clearDisplay(2);

  //Если минута 0
  if (minute == 0) {
    writeNumberOnDisplay(DISPLAY_COUNT - 1, 0);
    writeNumberOnDisplay(DISPLAY_COUNT - 2, 0);

    return;
  }

  //Если минута от 1 до 9, то подставляем 0 перед числом
  if (minute > 0 && minute < 10) {
    writeNumberOnDisplay(DISPLAY_COUNT - 1, minute);
    writeNumberOnDisplay(DISPLAY_COUNT - 2, 0);
  } else {
    while (minute != 0) {
      number = minute % 10;
      writeNumberOnDisplay(DISPLAY_COUNT - i, number);
      minute = minute / 10;
      i++;
    }
  }


}

/*
  Вывод часов на дисплей
*/
void writeHours(int hour) {
  int number;
  int i = 3;

  /*
     Очищаем экраны
  */
  lc.clearDisplay(1);
  lc.clearDisplay(0);

  //Если на часах 00
  if (hour == 0) {
    writeNumberOnDisplay(DISPLAY_COUNT - 3, 0);
    writeNumberOnDisplay(DISPLAY_COUNT - 4, 0);

    return;
  }

  //Если часы от 1 до 9, то подставляем 0 перед числом
  if (hour > 0 && hour < 10) {
    writeNumberOnDisplay(DISPLAY_COUNT - 3, hour);
    writeNumberOnDisplay(DISPLAY_COUNT - 4, 0);
  } else {
    while (hour != 0) {
      number = hour % 10;
      writeNumberOnDisplay(DISPLAY_COUNT - i, number);
      hour = hour / 10;
      i++;
    }
  }
}

/*
  Конвенрирует значения датчика освещенности в значения яркости для матрицы
*/
int adaptiveBrightness(int brightness) {
  if (brightness < 700) {
    return 15;
  }
  if (brightness >= 700 && brightness < 800) {
    return 15;
  }
  if (brightness >= 800 && brightness < 850) {
    return 12;
  }
  if (brightness >= 850 && brightness < 900) {
    return 9;
  }
  if (brightness >= 900 && brightness < 950) {
    return 6;
  }
  if (brightness >= 950 && brightness < 1000) {
    return 3;
  }
  if (brightness >= 1000 && brightness < 1050) {
    return 0;
  }
  if (brightness >= 1050) {
    return 0;
  }
}

void loop() {
  //Структура, содержащая знакчения даты и времени в текущий момент времени
  tmElements_t tm;

  buttonModeState = digitalRead(buttonModePin);

  unsigned long currentMillis = millis();

  if (buttonModeState == HIGH && flagTime == 0) {
    switch (MODE) {
      case MODE_TIME:
        MODE = MODE_HOURS;
        break;
      case MODE_HOURS:
        lc.shutdown(0, false);
        lc.shutdown(1, false);
        MODE = MODE_MINUTES;
        break;
      case MODE_MINUTES:
        lc.shutdown(2, false);
        lc.shutdown(3, false);
        MODE = MODE_TIME;
        break;
    }
    flagTime = 1;
  }

  if (buttonModeState == LOW && flagTime == 1) {
    flagTime = 0;
  }

  switch (MODE) {
    case MODE_TIME:
      if (currentMillis - previousTimeMillis > delaytime) {
        previousTimeMillis = currentMillis;

        if (RTC.read(tm)) {
        Serial.println(tm.Hour);
        Serial.println(tm.Minute);
        Serial.println(tm.Second);

        //Если настала новая минута
        if (tm.Second == 0) {
            writeHours(tm.Hour);
            writeMinutes(tm.Minute);
          }
        }      
      }
      
      break;
    case MODE_HOURS:
      buttonUpState = !digitalRead(buttonUpPin);
      buttonDownState = !digitalRead(buttonDownPin);

      if (buttonDownState == HIGH && flagHourDown == 0) {
        if (tm.Hour == 0) {
          tm.Hour = 23;
        } else {
          tm.Hour = tm.Hour - 1;
        }
        RTC.write(tm);
        writeHours(tm.Hour);
        flagHourDown = 1;
      }

      if (buttonDownState == LOW && flagHourDown == 1) {
        flagHourDown = 0;
      }

      if (buttonUpState == HIGH && flagHourUp == 0) {
        if (tm.Hour == 23) {
          tm.Hour = 0;
        } else {
          tm.Hour = tm.Hour+1;
        }
        RTC.write(tm);
        writeHours(tm.Hour);
        flagHourUp = 1;
      }

      if (buttonUpState == LOW && flagHourUp == 1) {
        flagHourUp = 0;
      }
      
      if (currentMillis - previousHoursMillis > delaytime) {
        previousHoursMillis = currentMillis;
        Serial.println(previousHoursMillis);
        if (shutdownHours == 0) {
          lc.shutdown(0, true);
          lc.shutdown(1, true);
          shutdownHours = 1;
          Serial.println(shutdownHours);
        } else {
          lc.shutdown(0, false);
          lc.shutdown(1, false);
          shutdownHours = 0;
          Serial.println(shutdownHours);
        }
      }
      break;
    case MODE_MINUTES:
      buttonUpState = !digitalRead(buttonUpPin);
      buttonDownState = !digitalRead(buttonDownPin);

      if (buttonDownState == HIGH && flagMinuteDown == 0) {
        if (tm.Minute == 0) {
          tm.Minute = 59;
        } else {
          tm.Minute = tm.Minute - 1;
        }
        RTC.write(tm);
        writeMinutes(tm.Minute);
        flagMinuteDown = 1;
      }

      if (buttonDownState == LOW && flagMinuteDown == 1) {
        flagMinuteDown = 0;
      }

      if (buttonUpState == HIGH && flagMinuteUp == 0) {
        if (tm.Minute == 59) {
          tm.Minute = 0;
        } else {
          tm.Minute = tm.Minute + 1;
        }
        RTC.write(tm);
        writeMinutes(tm.Minute);
        flagMinuteUp = 1;
      }

      if (buttonUpState == LOW && flagMinuteUp == 1) {
        flagMinuteUp = 0;
      }
      
      if (currentMillis - previousMinutesMillis > delaytime) {
        previousMinutesMillis = currentMillis;
        Serial.println(previousMinutesMillis);
        if (shutdownMinutes == 0) {
          lc.shutdown(2, true);
          lc.shutdown(3, true);
          shutdownMinutes = 1;
          Serial.println(shutdownMinutes);
        } else {
          lc.shutdown(2, false);
          lc.shutdown(3, false);
          shutdownMinutes = 0;
          Serial.println(shutdownMinutes);
        }
      }
      break;
  }
  //  photocellReading = analogRead(photocellPin);
  //  bright = adaptiveBrightness(photocellReading);
  //
  //  Serial.println(photocellReading);
  //  Serial.println(bright);

  //  lc.setIntensity(0, bright);
  //  lc.setIntensity(1, bright);
  //  lc.setIntensity(2, bright);
  //  lc.setIntensity(3, bright);

  //Если можно прочитать время с датчика

}
